//AGRADECIMENTO ESPECIAL AO MEU AMIGO CIUS, DA NOSSA INCRIVEL OHANA. 
//Estava com muitas dificuldades para entender o jogo da velha e prontamente ele me ajudou
//durante a semana, deixando de fazer as coisas dele que eram importantes.

/*   
        |  /\  | | |   _____   _____  |\  |
        | /  \ | | |   |____  |     | | \ |
        |/    \| | |___ ____| |_____| |  \|
              (GAME DEVELOPMENT)
        */
//Declarando váriaveis importantes
//thereisgame é a variavel que indica se o game está ativo
var thereisgame = true;
//variavel da vez      
var time = "X";  
// contador 
var count = 0 ;
//a função tem como objetivo a seleção do botão
function selecionar(botao)
{
     if(botao.innerHTML == "" && thereisgame == true) //se ainda houver jogo.. (thereisgame == true)
    {
         if (time == "X")  //Declarando as condições caso for X ou O
        {
 			botao.innerHTML = "X";
 			document.getElementById("indicadorDaVez").innerHTML = "O";
 			time = "O";
 			count++
 		}
         else if ( time == "O")
        {
 			botao.innerHTML = "O";
 			document.getElementById("indicadorDaVez").innerHTML = "X";
 			time = "X";
 			count++
 		}
         if (count == 9)
        {
 			document.getElementById("indicadorVencedor").innerHTML = "Deu VELHA MAH!";
 			document.getElementById("indicadorDaVez").innerHTML = "";
 		}
 		
 	}
	winner()	
}

function resetar() //função que é inicializada, caso o jogador clique no botão, ela retorna aos status do inicio do jogo
{
    for(var i = 0; i < 9; i++)
    {
        document.getElementsByClassName("casa")[i].innerHTML = "";
    }
    document.getElementById("indicadorDaVez").innerHTML = "X";
    document.getElementById("indicadorVencedor").innerHTML = "";
    count = 0;
    time = "X";
    thereisgame = true;
}

function winner() //definiremos os casos de vitória
{
//O SEGREDO É VISUALIZAR OS BOTOES COMO SE FOSSEM UMA MATRIZ DE 3x3, começando do 0 até 8    

	//0 1 2
	if(document.getElementsByClassName("casa")[0].innerHTML != '' && document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[1].innerHTML
     && document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[2].innerHTML )
    {
		document.getElementById("indicadorVencedor").innerHTML = "O vencedor é  "+ document.getElementsByClassName("casa")[0].innerHTML;
         thereisgame = false;
    }
	//3 4 5
	else if(document.getElementsByClassName("casa")[3].innerHTML != '' && document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[4].innerHTML
     && document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[5].innerHTML )
    {
		document.getElementById("indicadorVencedor").innerHTML = "O vencedor é  "+ document.getElementsByClassName("casa")[3].innerHTML;
        thereisgame = false;
    }
	//6 7 8
	else if(document.getElementsByClassName("casa")[6].innerHTML != '' && document.getElementsByClassName("casa")[6].innerHTML == document.getElementsByClassName("casa")[7].innerHTML
     && document.getElementsByClassName("casa")[7].innerHTML == document.getElementsByClassName("casa")[8].innerHTML )
    {
		document.getElementById("indicadorVencedor").innerHTML = "O vencedor é  "+ document.getElementsByClassName("casa")[6].innerHTML;
        thereisgame = false;
    }
	//0 3 6
	else if(document.getElementsByClassName("casa")[0].innerHTML != '' && document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[3].innerHTML
     && document.getElementsByClassName("casa")[3].innerHTML == document.getElementsByClassName("casa")[6].innerHTML )
    {
		document.getElementById("indicadorVencedor").innerHTML = "O vencedor é  "+ document.getElementsByClassName("casa")[0].innerHTML;
        thereisgame = false;
    }
	//1 4 7
	else if(document.getElementsByClassName("casa")[1].innerHTML != '' && document.getElementsByClassName("casa")[1].innerHTML == document.getElementsByClassName("casa")[4].innerHTML
     && document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[7].innerHTML )
    {
		document.getElementById("indicadorVencedor").innerHTML = "O vencedor é  "+ document.getElementsByClassName("casa")[1].innerHTML;
        thereisgame = false;
    }
	//2 5 8
	else if(document.getElementsByClassName("casa")[2].innerHTML != '' && document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[5].innerHTML
     && document.getElementsByClassName("casa")[5].innerHTML == document.getElementsByClassName("casa")[8].innerHTML )
    {
		document.getElementById("indicadorVencedor").innerHTML = "O vencedor é  "+ document.getElementsByClassName("casa")[2].innerHTML;
        thereisgame = false;
    }
    //0 4 8
	else if(document.getElementsByClassName("casa")[0].innerHTML!= '' &&document.getElementsByClassName("casa")[0].innerHTML == document.getElementsByClassName("casa")[4].innerHTML
    && document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[8].innerHTML )
   {
       document.getElementById("indicadorVencedor").innerHTML = "O vencedor é  "+ document.getElementsByClassName("casa")[0].innerHTML;
       thereisgame = false;
   }
  	//2 4 6
  	else if(document.getElementsByClassName("casa")[2].innerHTML != '' && document.getElementsByClassName("casa")[2].innerHTML == document.getElementsByClassName("casa")[4].innerHTML
     && document.getElementsByClassName("casa")[4].innerHTML == document.getElementsByClassName("casa")[6].innerHTML )
    {
		document.getElementById("indicadorVencedor").innerHTML = "O vencedor é : "+ document.getElementsByClassName("casa")[2].innerHTML;
        thereisgame = false;
    }
}
